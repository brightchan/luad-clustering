#!/bin/bash
### Review the ./para.sh for variables before running.
# No need to make new dir.
###

source ./para.sh

#STAR parameter groups
#Note: For gz or other compressed file, the script need to be adjust with "--readFilesCommand gunzip -c".
STARparCommon=" --genomeDir $indexDir --sjdbGTFfile $gtf"
STARparRun=" --runThreadN $TN" #Set Num of threads. Try if --genomeLoad LoadAndKeep" works. For RSEM, add --quantMode TranscriptomeSAM
STARparOut=" --outSAMtype BAM SortedByCoordinate" #output sorted BAM

#STAR for each fq/fa
echo "********************STAR started*******************"
if ! test -d "$outp/STAR_out"; then
    mkdir $outp/STAR_out
fi
find $inp -iname "*.f?"|sort >fastq.list
if [ "$pairend"=true ];
then
    while read reads;do
        read reads2
        STARparIn=" --readFilesIn $reads $reads2" 
        bname=$(basename $reads)
        id="${bname%_1.f?}" #sample id w/o suffix
        mkdir $outp/STAR_out/$id
        STARparOutdir=" --outFileNamePrefix $outp/STAR_out/$id/${id}."
        $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
    done<fastq.list #iterate over the list of fastq and run STAR 
else
    while read reads;do
        STARparIn=" --readFilesIn $reads"
        bname=$(basename $reads)
        id="${bname%_1.f?}" #sample id w/o suffix
        mkdir $outp/STAR_out/$id
        STARparOutdir=" --outFileNamePrefix $outp/STAR_out/$id/${id}."
        $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
    done<fastq.list
fi
rm fastq.list
echo "**********************STAR DONE**********************"
date
