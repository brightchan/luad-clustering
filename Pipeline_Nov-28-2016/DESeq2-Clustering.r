#!/bin/Rscript

##### DESeq2 normalization & clustering
# Part of the STAR-featureCounts-DESeq2-R clustering pipeline.
# DESeq2 "estimateSizeFactors" normalization used for raw counts from featureCounts.
# Cluster with centroid from Wilkerson et.al. PLOS One 2012, or by concensusClusterPlus. 
# geneID used for comparing.
# Required packages: mygene, DESeq2, dplyr, gplots
##### by CJB Nov-2016

############## Set the parameters below before running ######################

setwd("C:\\LUAD_Clustering\\") # Set the working dir for this script
counttable=".\\Output\\Nov-19-2016\\featureCounts_out\\count_table.txt" # path of the count_table
Centroid=".\\Data\\wilkerson.2012.LAD.predictor.centroids.csv" # path of the wilkerson centroid
outp=".\\Output\\Nov-28-2016\\" # Output folder
dir.create(file.path(outp),showWarnings = F)

########################### End of setting###################################


### Useful functions
# Sum read count from the duplicated rows then delete them. Index in 1st column.
sumNdelete <- function(x,a){ #x:dataframe; a:vector of row No.
  for (i in a[2:length(a)]){
    x[a[1],2:ncol(x)] <- x[a[1],2:ncol(x)]+x[i,2:ncol(x)]
  }
  x[a[2:length(a)],] <- NA
  return(x)
}

# Change rowname from ENSEMBL to geneID. Omit unmatched row. Sum and Delete rows with same geneID.
# Input dataframe output the same.
rname_EN2gID <- function(x){
  library("mygene")
  EnID <- gsub("\\..*$","",row.names(x))
  temp <- queryMany(EnID, fields="entrezgene",species="human") # DF with query,_id,notfound
  x <- x[complete.cases(temp$"entrezgene"),]
  gID <- temp[complete.cases(temp$"entrezgene"),"entrezgene"]
  x <- cbind(gID,x)
  dup_gID <- which(duplicated(gID)) # Row No. of duplicated w/o first element
  while (length(dup_gID)>0) {
    a <- which(gID==gID[dup_gID[1]]) # All row No. of duplicated
    x <- sumNdelete(x,a)
    dup_gID <- dup_gID[!dup_gID %in% a]
  }
  x <- na.omit(x)
  row.names(x) <- x[,1]
  x <- x[,-1]
  tempn <- sum(na.omit(temp$notfound=="TRUE"))
  message(as.character(tempn)," rows ommited without hit:")
  print(temp[complete.cases(temp$notfound),"query"]) # Echo the ENSEMBLID of non-hit
  return(x)
}

# Change rowname from symbol to geneID. Input dataframe output the same.
rname_symb2gID <- function(x){
  library("mygene")
  temp <- queryMany(row.names(x),scopes="symbol", fields="entrezgene",species="human") # DF with query,_id,notfound
  temp <- temp[! duplicated(temp$query),] # remove duplicated gene (some symbol match to multiple id)
  x <- x[complete.cases(temp$"entrezgene"),] # omit non-hit genes
    row.names(x) <- temp[complete.cases(temp$"entrezgene"),"entrezgene"]
  tempn <- sum(na.omit(temp$notfound=="TRUE"))
  message(as.character(tempn)," rows ommited without hit:")
  print(temp[complete.cases(temp$notfound),"query"]) # Echo the ENSEMBLID of non-hit
  return(x)
}

# Median centering of each gene as rows. Input num.matrix output the same.
medianCentered <- function(x) { 
  med <- apply(x,1,median)
  t(scale(t(x),center = med,scale = F)) #check for scale:normalize to 1 var?
}

# Get subset of x according to y by rownames and order by rownames
subset_rowname <- function(x,y) { 
  library(dplyr)
  subx <- semi_join(x %>%  
                      mutate(rowname=row.names(x)), #add col "rowname" for ref
                    y %>% 
                      mutate(rowname=row.names(y)), 
                    by="rowname", copy=T)
  row.names(subx) <- subx[,"rowname"]
  subx$rowname <- NULL
  subx <- subx[order(row.names(subx)),]
  message("Rows:", as.character(nrow(x))," -> ",as.character(nrow(subx)))
  return(subx)
}



###### Main ######

### 1. Read data
# Read the raw count table from featureCounts
countdata <- read.table(counttable,header = F, row.names = 1, stringsAsFactors=FALSE) 
# Remove first five columns (chr, start, end, strand, length)
countdata <- countdata[ ,6:ncol(countdata)] 
# Remove pre-and suffix from STAR output filenames, convert to numeric dataframe
colnames(countdata) <- apply(as.vector(countdata[1,]),1,basename)
colnames(countdata) <- gsub("\\.Aligned\\.sortedByCoord\\.out\\.bam$", "", colnames(countdata))
countdata2 <- as.data.frame(apply(countdata[-1,],2,as.numeric),row.names= row.names(countdata[-1,]))
# Change rowname from EnsemblID to geneID, add up replicated genes
countdata_gID <- rname_EN2gID(countdata2)
countdata_gID <- as.matrix(countdata_gID)
mode(countdata_gID) <- "numeric"
# Construct single column coldata (for DESeq2)
coldata <- data.frame(colnames(countdata_gID))


### 2. DESeq2 Normalization.
# Return read counts normalized by dividing raw count by sizefactor, then log2 transformed and center by median
library("DESeq2")
dds <- DESeqDataSetFromMatrix(countdata_gID, coldata, ~1) #~1:no design
dds <- estimateSizeFactors(dds)
norm_counts <- log2(counts(dds, normalized=TRUE)+1) # add 1 pseudocount
mcd_norm_counts <- as.data.frame(medianCentered(norm_counts))
colnames(mcd_norm_counts) <- colnames(countdata_gID)
## ALTERNATIVE: Normalize by normTransform with pseudocount
# min_countdata <- min(countdata[countdata>0]) # Add pseudocount with min value of the whole matrix
# normtrans_counts <- assay(normTransform(dds, f = log2, pc = min_countdata)) 
# mcd_normtrans_counts <- as.data.frame(medianCentered(normtrans_counts))


### 3. Subset genes in wilkerson's centroid list
# Read centroids from Wilkerson et.al. PLOS One 2012, gene-median centered
WCentroids <- read.csv(Centroid, row.names=1) 
WCentroids_gID <- rname_symb2gID(WCentroids)
sub_mcd_norm_counts <- subset_rowname(mcd_norm_counts,WCentroids_gID)
sub_WCentroids_gID <- subset_rowname(WCentroids_gID,sub_mcd_norm_counts)


### 4. Assign subtypes according to centroids
# Generate correlation matrix btw data and centroid
cormtx <- cor(sub_mcd_norm_counts,sub_WCentroids_gID)
colnames(cormtx) <- colnames(sub_WCentroids_gID)
#assign subtype
subtype <- data.frame(Sample= row.names(cormtx), Subtype=colnames(cormtx)[max.col(cormtx)])
#sort by subtype
write.table(subtype,file = paste0(outp,"Subtype.txt"), quote = F, sep="\t", eol="\n", row.names = F)


### 5. Heatmap of subset genes with subtypes
counts_subtype <- rbind(sub_mcd_norm_counts,subtype=subtype$Subtype)
counts_subtype <- counts_subtype[,order(counts_subtype["subtype",])]
csc2 <- c("deepskyblue3","ivory","coral")[as.factor(sort(subtype$Subtype))]
my_palette <- colorRampPalette(c("deepskyblue3","ivory","coral"))
breaks <- seq(-1.8,1.8,0.2)^3+0.5 # Adjust this color breaks for better look
library(gplots)
tiff(paste0(outp,"heatmap.tiff"),width = 1000, height = 800)
heatmap.2(as.matrix(counts_subtype)[1:(nrow(counts_subtype)-1),], trace='none', 
          breaks = breaks,col=my_palette,
          Rowv = T,Colv=F, ColSideColors = csc2, #default dendro: hclust & dist
          density.info='histogram', key=T, keysize=0.8, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = NA)
dev.off()








# ###### Code for testing. DO NOT USE.
# ### Read TCGA Level 3 data
# source(".\\Script\\Nov-21-2016\\ExtractTCGAcounts.R")
# countdata <- count_table_TCGA("Data\\unc.edu_LUAD.IlluminaHiSeq_RNASeqV2.Level_3.1.5.0.luad2014\\",
#                                     "\\.rsem\\.genes\\.results$","raw_count")
# 
# ###check correctness
# tcgaLUAD2012_subtype <- read.csv(".\\Data\\tcga.luad.gene.expression.subtypes.20121025.csv")
# row.names(tcgaLUAD2012_subtype) <- tcgaLUAD2012_subtype$sampleId
# tcgaLUAD2012_subtype$sampleId <- NULL
# #assign number to subtype
# tem_factor <- as.factor(tcgaLUAD2012_subtype$expression_subtype)
# tcgaLUAD2012_subtype$expression_subtype <- factor(tem_factor, levels = c("TRU","Proximal Proliferative","Proximal Inflammatory"))
# tcgaLUAD2012_subtype <- cbind(tcgaLUAD2012_subtype,subtype_num=as.numeric(tcgaLUAD2012_subtype$expression_subtype))
# tcgaLUAD2012_subtype <- tcgaLUAD2012_subtype[order(row.names(tcgaLUAD2012_subtype)),]
# #compare with my subtype
# ctd_subtype <- as.data.frame(subwilk_tcgaLUAD2012_subtype["subtype",])
# ctd_subtype <- ctd_subtype[,order(colnames(ctd_subtype))]
# sum(ctd_subtype - tcgaLUAD2012_subtype$subtype_num)
# 
# 
# ### Compare selected genes with different clustering !PROBLEMATIC CODE!
# genelist <- c("SFTPC","DMBT1","FOLR1","DUSP4","FGL1","TDG","PLAU","G0S2","CXCL10")
# lmat = rbind(c(0,3),c(2,1),c(4,0))
# lwid = c(0.8,5)
# lhei = c(1,5,0.5)
# par(cex.main=0.8)
# heatmap.2(as.matrix(subwilk_tcgaLUAD2012)[genelist,], trace='none', breaks = breaks,
#           Rowv = F,Colv="Rowv",dendrogram = "none", col=my_palette, density.info="none", 
#           key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA, main="Hierarchical",
#           labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
# heatmap.2(as.matrix(subwilk_tcgaLUAD2012_k)[genelist,], trace='none', 
#           breaks = breaks,col=my_palette, density.info="none",
#           Rowv = F,Colv="Rowv", dendrogram = "none", #ColSideColors = csc,
#           key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA,
#           labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
# title("k-means", line=1.5)
# heatmap.2(as.matrix(subwilk_tcgaLUAD2012_subtype)[genelist,], trace='none', 
#           breaks = breaks,col=my_palette, density.info="none",
#           Rowv = F,Colv="Rowv", dendrogram = "none", #ColSideColors = csc2,
#           key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA,
#           labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
# title("Centroids", line=1.5)
# 
# 
