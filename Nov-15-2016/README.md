##Work flow from bam to count table with TCGA work flow

The homemade script, RSEM.sh, takes a list of bam files, uses RSEM to generate read counts for each bam and outputs a summarized count table with genes in rows and samples in columns (count_table.txt).

### NOTE:
1. Change the all variables according to the instruction below before runing the RSEM.sh script.
2. BAM file required should be "aligned to transcriptome" instead to genome: when using STAR, use --quantMode TranscriptomeSAM option. File names should be their sample ID.
3. To run the script, install Bowtie 2 and RSEM. 
    - For RSEM, download and extract the file from https://github.com/deweylab/RSEM/archive/v1.3.0.tar.gz 
    - For bowtie2, check bowtie-bio.sourceforge.net/bowtie2/index.shtml Make and install accordingly.
* * *
The script processes all the following 3 steps:
###Step 1: Build Reference for RSEM
Download human genome with fa & GTF file.
Change the $genome and $GTF to path of the two files.
Change the $bt2path to bowtie2
Change the $humanref to the path with file prefix of the output RSEM reference.

###Step 2: Generate read counts from each bam file
Change $bamlist to the file containing path of bam files to be analysed. 
Change $output to the directory for the output count table.
Change $p to set num of cores.

###Step 3:Generate a summarized count table from outputs of all bam files

