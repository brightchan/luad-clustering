#!/bin/bash
#This script takes a file with list of bam, uses RSEM to generate read counts for each bam and outputs a summarized count table with genes in rows and samples in columns (count_table.txt).


#Review the following before running. No need to make new dir before running.
bt2path=../../Software/bowtie2-2.2.6 #path to bowtie2
genome=../../Data/GRCh38.p7.genome.fa #genome.fa
GTF=../../Data/gencode.v25.annotation.gtf #annotation.gtf 
humanref=../../Output/Nov-17-2016/RSEMref_hg38v25/RSEMref_hg38v25 #output path and prefix for reference
bamlist=../../Data/test_bam/list.txt #list of the input bam
output=../../Output/Nov-17-2016 #final output path
p=4 #No. of cores

#Generate reference
if [ ! -d "$humanref"]; then
	mkdir $(dirname $humanref)
	rsem-prepare-reference \
	--gtf $GTF --bowtie2 --bowtie2-path $bt2path \
	$genome $humanref
fi

#Generate counts from BAM
if [ ! -d "$output"]; then
	mkdir $output
	mkdir $output/log
fi
for f in $(cat $bamlist); do
  bname=$(basename $f)
  id="${bname%.*}" #sample id w/o ".bam"
  rsem-calculate-expression -p $p --paired-end \
	  --bam --estimate-rspd --time \
	  --append-names --output-genome-bam \
	  $f $humanref $output &> $output/log/${id}.rsem.log #record run log
done

#Generate summarized count table
awk 'NR>1{print $1}' $output/${id}.gene.results \
	|sort > count_temp #initialize gene list for combining below. count_temp for combining new columns
find $output -iname "*.genes.results">result.list
echo "transcript_id" > header #initialize header
for f in $(cat result.list);do
	basename "${f%.*.*}" >> header #get sample ID as header
	#combine the $6 columns(should be TPM). NA enabled.
	join -a1 -a2 -j1 -e NA -o auto count_temp <(awk 'NR>1{print $1"\t"$6}' $f|sort -k 1,1) >temp
	cat temp > count_temp
done
#generate header in one line
paste -s -d "\t" header>$output/count_table.txt 
cat count_temp>>$output/count_table.txt

rm count_temp temp header result.list
cat RSEM.sh > $output/run.script
echo "***************************RSEM.sh DONE**************************"
date
