###### Find clusters from TCGA LUAD RNASeq ######

### Read tables, genes as rows. Centering with gene median
setwd("C:\\My Documents\\destop\\LUAD Clustering")
library(dplyr)
wilkersonCentroids <- read.csv("\\Data\\wilkerson.2012.LAD.predictor.centroids.csv",row.names=1) #from Wilkerson et.al. PLOS One 2012
tcgaLUAD2012 <- read.csv("\\Data\\tcga.luad.rnaseq.20121025.csv",row.names = 1) #from TCGA Nature 2014, log2 transformed

#for median centering of each gene as rows
medianCentered <- function(x) { 
  med <- apply(x,1,median)
  t(scale(t(x),center = med,scale = F)) #check for scale:normalize to 1 var?
}
mcd_tcgaLUAD2012 <- as.data.frame(medianCentered(tcgaLUAD2012))
#subset genes in wilkerson's centroid list
subwilk_tcgaLUAD2012 <- semi_join(mcd_tcgaLUAD2012 %>%  
                                  mutate(rowname=row.names(mcd_tcgaLUAD2012)), #add col "rowname" for ref
                              wilkersonCentroids %>% 
                                  mutate(rowname=row.names(wilkersonCentroids)), 
                              by="rowname", copy=T)
row.names(subwilk_tcgaLUAD2012) <- subwilk_tcgaLUAD2012[,"rowname"]
subwilk_tcgaLUAD2012$rowname <- NULL #CHECK!
#order by rownames
subwilk_tcgaLUAD2012 <- subwilk_tcgaLUAD2012[order(row.names(subwilk_tcgaLUAD2012)),]

###### Clustering w/o centroids
### heatmap with hieracchical clustering
library(gplots)
breaks <- seq(-1.8,1.8,0.2)^3+0.5 # color breaks
my_palette <- colorRampPalette(c("deepskyblue3","ivory","coral"))
heatmap.2(as.matrix(subwilk_tcgaLUAD2012), trace='none', breaks = breaks,col=my_palette,
          Rowv = T,Colv="Rowv",dendrogram = "column", #default dendro: hclust & dist
          density.info='histogram', key=T, keysize=0.8, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = NA)

### heatmap with k-mean clustering
kmc <- function(mat,k){ #add k-mean info to the end of the matrix
  mat <- t(mat)
  tem <- kmeans(mat,k)
  head(tem)
  mat <- as.data.frame(cbind(mat,cluster=tem$cluster))
  mat_sort <- mat[order(mat$cluster),]
  return(t(mat_sort))
}
subwilk_tcgaLUAD2012_k <- kmc(as.matrix(subwilk_tcgaLUAD2012),3)
csc <- c("deepskyblue3","ivory","coral")[as.factor(subwilk_tcgaLUAD2012_k[nrow(subwilk_tcgaLUAD2012_k),])]
heatmap.2(as.matrix(subwilk_tcgaLUAD2012_k)[1:(nrow(subwilk_tcgaLUAD2012_k)-1),], trace='none', 
          breaks = breaks,col=my_palette,
          Rowv = T,Colv=F, ColSideColors = csc, #default dendro: hclust & dist
          density.info='histogram', key=T, keysize=0.8, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = NA)

### using ConsensusClusterPlus
library(ConsensusClusterPlus)
title="LUAD2012_ConsensusCluster_sub"
results = ConsensusClusterPlus(as.matrix(subwilk_tcgaLUAD2012),maxK=6,reps=1000,pItem=0.8,pFeature=0.8,
                               title=title,clusterAlg="hc",distance="pearson", plot="png")


###### Assign subtypes according to centroids
###subset wilkerson centroids
wilctd_sub <- semi_join(wilkersonCentroids %>% 
                          mutate(rowname=row.names(wilkersonCentroids)),
                        mcd_tcgaLUAD2012 %>%  
                          mutate(rowname=row.names(mcd_tcgaLUAD2012)), 
                        by="rowname", copy=T)
row.names(wilctd_sub) <- wilctd_sub$rowname
wilctd_sub$rowname <- NULL
#order by rownames
wilctd_sub <- wilctd_sub[order(row.names(wilctd_sub)),]
#Generate correlation matrix btw data and centroid
cormtx <- cor(subwilk_tcgaLUAD2012,wilctd_sub)
#cormtx <- cor(subwilk_tcgaLUAD2012,wilctd_sub)^2
colnames(cormtx) <- colnames(wilctd_sub)
#assign subtype
subtype <- max.col(cormtx)
subtype <- setNames(subtype,colnames(cormtx)[max.col(cormtx)])
subwilk_tcgaLUAD2012_subtype <- rbind(subwilk_tcgaLUAD2012,subtype=subtype)
#sort by subtype
subwilk_tcgaLUAD2012_subtype <- subwilk_tcgaLUAD2012_subtype[,order(subtype)]
#heatmap with subtyping
csc2 <- c("deepskyblue3","ivory","coral")[as.factor(sort(subtype))]
my_palette <- colorRampPalette(c("deepskyblue3","ivory","coral"))
breaks <- seq(-1.8,1.8,0.2)^3+0.5 # color breaks
heatmap.2(as.matrix(subwilk_tcgaLUAD2012_subtype)[1:(nrow(subwilk_tcgaLUAD2012_subtype)-1),], trace='none', 
          breaks = breaks,col=my_palette,
          Rowv = T,Colv=F, ColSideColors = csc2, #default dendro: hclust & dist
          density.info='histogram', key=T, keysize=0.8, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = NA)

###check correctness
tcgaLUAD2012_subtype <- read.csv("\\Data\\tcga.luad.gene.expression.subtypes.20121025.csv")
row.names(tcgaLUAD2012_subtype) <- tcgaLUAD2012_subtype$sampleId
tcgaLUAD2012_subtype$sampleId <- NULL
#assign number to subtype
tem_factor <- as.factor(tcgaLUAD2012_subtype$expression_subtype)
tcgaLUAD2012_subtype$expression_subtype <- factor(tem_factor, levels = c("TRU","Proximal Proliferative","Proximal Inflammatory"))
tcgaLUAD2012_subtype <- cbind(tcgaLUAD2012_subtype,subtype_num=as.numeric(tcgaLUAD2012_subtype$expression_subtype))
tcgaLUAD2012_subtype <- tcgaLUAD2012_subtype[order(row.names(tcgaLUAD2012_subtype)),]
#compare with my subtype
ctd_subtype <- as.data.frame(subwilk_tcgaLUAD2012_subtype["subtype",])
ctd_subtype <- ctd_subtype[,order(colnames(ctd_subtype))]
sum(ctd_subtype - tcgaLUAD2012_subtype$subtype_num)


### Compare selected genes with different clustering
## PROBLEMATIC CODE
genelist <- c("SFTPC","DMBT1","FOLR1","DUSP4","FGL1","TDG","PLAU","G0S2","CXCL10")
lmat = rbind(c(0,3),c(2,1),c(4,0))
lwid = c(0.8,5)
lhei = c(1,5,0.5)
par(cex.main=0.8)
heatmap.2(as.matrix(subwilk_tcgaLUAD2012)[genelist,], trace='none', breaks = breaks,
          Rowv = F,Colv="Rowv",dendrogram = "none", col=my_palette, density.info="none", 
          key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA, main="Hierarchical",
          labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
heatmap.2(as.matrix(subwilk_tcgaLUAD2012_k)[genelist,], trace='none', 
          breaks = breaks,col=my_palette, density.info="none",
          Rowv = F,Colv="Rowv", dendrogram = "none", #ColSideColors = csc,
          key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
title("k-means", line=1.5)
heatmap.2(as.matrix(subwilk_tcgaLUAD2012_subtype)[genelist,], trace='none', 
          breaks = breaks,col=my_palette, density.info="none",
          Rowv = F,Colv="Rowv", dendrogram = "none", #ColSideColors = csc2,
          key=T, keysize=0.5, key.xlab=NA, key.ylab=NA, key.title=NA,
          labCol = NA, labRow = genelist, lwid=lwid, lhei=lhei, lmat=lmat)
title("Centroids", line=1.5)