##RNA-seq subtype detection
Input TCGA RNA-seq read count, output heatmap and clusters of LUAD using existing centroid with R.
###1.Subset centroid genes according to wilkerson's list.
###2.Data clean up
RPKM to log2 value.0 to NA. Center to gene median by scale(). (additional: At least 1 obs. > certain number; Max �C Min >= 1).
###3.Heatmap2: Hierarchical & k-means cluster.
###4.ConsensusClusterPlus: input expression levels, gain unsupervised clusters.
###5.Assign subtypes to each sample by Pearson's correlation.
