## STAR-featureCounts-DESeq2-Clustering Pipeline
### Input: reads(fq)/alignment(bam)/count table(txt); 
### Output: Subtype(subtype.txt) and Heatmap(heatmap.pdf).

This pipeline uses STAR as aligner, featureCounts for read counting,

DESeq2 for read count normalization, and Pearson correlation to assign

LUAD samples to Wilkerson's centroid (10.1371/journal.pone.0036530).

Here is the workfolw from fq input to the output:

1. Install STAR and featureCounts as command line executable.

After installation, locate the STAR and featureCounts executable,

and fill in the path to para.sh

2. Prepare STAR genome index.

Refer to STAR manual, generate or download the genome index for STAR. 

Fill in the path to para.sh

3. Set other parameters in the para.sh

Set input and output folder, No. of thread to be used, etc.

Check para.sh for detail.

4. Run the STAR-featureCounts.sh.

Execute the script in bash. 

For alignment(bam) input, change the $inp in para.sh,

and execute the featureCounts.sh.

5. Install necessary R packages.

Install "mygene", "DESeq2" from Bioconductor,

"dplyr", "gplots" by install.packages.

6. Set and run the DESeq2-Cluster.r

In the begining of the script, fill in 

a)the path of count table output (count_table.txt)

b)working dir, and c)Wilkerson's centroid (WCentroid.txt).

Use command line ./DESeq2-Cluster.r or run the script in RStudio.

*Note

Most parameters of STAR and featureCounts are by default. Try to use "LoadAndKeep" for STAR under $STARparRun for better performance. For pairend reads, fragment length set to be between 50-600.

Most GTF use EnsemblID as gene_id. In the R scrpit, conversion to entrazID is done before comparing with Wilkerson's centroid, which uses gene symbol and is converted to entrazID. The conversion may combine duplicated genes or delete no-hit genes.

Raw read counts are normalized by dividing raw count by "sizefactor" from DESeq2, which account for library size and expression levels (http://precedings.nature.com/documents/6837/version/2/files/npre20126837-2.pdf). Then the normalized counts are log2 transformed and center by median before comparing to centroids. For detail, check http://www.nature.com/nature/journal/v511/n7511/extref/nature13385-s1.pdf.


