#!/bin/bash

###### This script uses STAR and featureCounts to generate read counts.
# Input: a folder of fq/fa files; Output: a summarized count table (count_table.txt)
# with genes in rows and sample ID in columns (count_table.txt).
# Two folders (STAR_out & featureCounts_out) will be made containing the results.
# Modify the ./para.sh for variables before running. No need to make new dir.
###### by CJB Nov-2016

source ./para.sh

#STAR parameter groups
#Note: For gz or other compressed file, the script could to be adjust with "--readFilesCommand gunzip -c".
STARparCommon=" --genomeDir $indexDir --sjdbGTFfile $gtf"
STARparRun=" --runThreadN $TN" # Set Num of threads. Try if "--genomeLoad LoadAndKeep" works. For RSEM, add --quantMode TranscriptomeSAM
STARparOut=" --outSAMtype BAM SortedByCoordinate" #output sorted BAM

#STAR for each fq/fa
echo "********************STAR started*********************"
if [ ! -d "$outp/STAR_out" ]; then
    mkdir $outp/STAR_out
fi
find $inp -iname "*.f?"|sort >fastq.list
if [ "$pairend"=true ];
then
    while read reads;do
        read reads2
        STARparIn=" --readFilesIn $reads $reads2" 
        bname=$(basename $reads)
        id="${bname%_1.f?}" #sample id w/o suffix
        mkdir $outp/STAR_out/$id
        STARparOutdir=" --outFileNamePrefix $outp/STAR_out/$id/${id}."
        echo $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
        $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
    done<fastq.list #iterate over the list of fastq and run STAR 
else
    while read reads;do
        STARparIn=" --readFilesIn $reads"
        bname=$(basename $reads)
        id="${bname%_1.f?}" #sample id w/o suffix
        mkdir $outp/STAR_out/$id
        STARparOutdir=" --outFileNamePrefix $outp/STAR_out/$id/${id}."
        echo $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
        $STAR $STARparCommon $STARparRun $STARparOut $STARparIn $STARparOutdir
    done<fastq.list
fi
echo "**********************STAR DONE**********************"
date


echo "****************featureCounts started****************"
#featureCounts parameter groups
if [ ! -d "$outp/featureCounts_out" ]; then
    mkdir $outp/featureCounts_out
fi
fCpar=" -a $gtf -G $genome -t exon -g gene_id -o $outp/featureCounts_out/count_table.txt -T $TN" #-t: feature type of the gtf; -g: attr. type for grouping feature into meta-features genes); -T:Num of threads.

#featureCounts for each bam
find $outp -iname "*Aligned.sortedByCoord.out.bam" > temp
bamlist=$(paste -s -d " " temp) #generate bamlist in one line
if [ "$pairend"=true ];
then
    fCparPair=" -p -B -P -C -d 50 -D 600" #par for pairend reads. -P-d-D:min & max fragment length; -B:both end mapped required; -C:not count chimeric fragment
    echo $featureCounts $fCpar $fCparPair $bamlist
    $featureCounts $fCpar $fCparPair $bamlist
else
    echo $featureCounts $fCpar $bamlist
    $featureCounts $fCpar $bamlist
fi
rm temp fastq.list
cat para.sh > $output/run.script
cat STAR-featureCounts.sh >> $output/run.script
echo "********************STAR-featureCounts.sh DONE********************"
date
