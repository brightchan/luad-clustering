#!/bin/bash

for i in {1..5}; do
    sed -n $[(i-1)*10000+1],$[i*10000]p ../218_2.fq > ./input/sub_${i}_2.fq
done
