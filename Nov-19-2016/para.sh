#!/bin/bash
### This file is required by STAR-featureCounts.sh, STAR.sh, and featureCounts.sh.
# Modify this file before executing the pipeline.
### by CJB Nov-2016 

featureCounts=/cygdrive/c/LUAD_Clustering/Software/subread-1.5.1-source/bin/featureCounts.exe #location of the featureCounts program
STAR=/cygdrive/c/LUAD_Clustering/Software/STAR/bin/Linux_x86_64/STAR #location of the STAR program
wd=/cygdrive/c/LUAD_Clustering #working dir
inp=$wd/Data/test_subfq #Dir of all fq/bam for analysis.
outp=$wd/Output/Nov-19-2016 #Dir for output folders
TN=2 #Number of Thread

#Parameters for STAR
indexDir=$wd/Data/GRCh38/STARind #Dir of STAR index
genome=$wd/Data/GRCh38.p7.genome.fa #genome.fa
gtf=$wd/Data/gencode.v25.annotation.gtf #GTF
pairend=true #pair end or not

