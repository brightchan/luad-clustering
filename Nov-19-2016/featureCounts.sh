#!/bin/bash
### Script for featureCounts only
# Review the ./para.sh for variables before running.
### No need to make new dir.

source ./para.sh

echo "****************featureCounts started****************"
#featureCounts parameter groups
if [ ! -d "$outp/featureCounts_out" ]; then
    mkdir "$outp/featureCounts_out"
fi
fCpar=" -a $gtf -G $genome -t exon -g gene_id -o $outp/featureCounts_out/count_table.txt -T $TN" #-t: feature type of the gtf; -g: attr. type for grouping feature into meta-features genes; -T:Num of threads.

#featureCounts for each bam
find "$inp" -iname "*Aligned.sortedByCoord.out.bam" > temp
bamlist=$(paste -s -d " " temp) #generate bamlist in one line
if [ "$pairend"=true ];
then
    fCparPair=" -p -B -P -C -d 50 -D 600" #par for pairend reads. -P-d-D:min & max fragment length; -B:both end mapped required; -C:not count chimeric fragment
    $featureCounts $fCpar $fCparPair $bamlist
else
    $featureCounts $fCpar $bamlist
fi

rm temp 
cat para.sh > "$output/run.script"
cat STAR-featureCounts.sh >> "$output/run.script"
echo "********************featureCounts DONE********************"
date
